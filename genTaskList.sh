#!/usr/bin/env bash
#              __ _ _    _                 _            
#             / _(_) |  | |               (_)           
#   _ __ ___ | |_ _| |  | | __ _ _ __ _ __ _  ___  _ __ 
#  | '__/ _ \|  _| | |/\| |/ _` | '__| '__| |/ _ \| '__|
#  | | | (_) | | | \  /\  / (_| | |  | |  | | (_) | |   
#  |_|  \___/|_| |_|\/  \/ \__,_|_|  |_|  |_|\___/|_|   
#                                                     
#                                                                                            
# Author: Clément Martinache
#
#Small script to generate the task list so it can be parsed easily.
#
# Inspired by rofi-bluetooth (https://github.com/ClydeDroid/rofi-bluetooth/blob/master/rofi-bluetooth)
#
# Depends on:
#   Arch repositories: rofi, task

#First take care of padding
max_desc_len=11
max_project_len=7
max_dueDate_len=3

tasks_ids=$(task _ids)
for task_id in $tasks_ids
do

	task_desc=$(task "$task_id" information | grep "Description" | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	task_project=$(task "$task_id" information | grep "Project" | head -n 1 | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	task_due_date=$(task "$task_id" information | grep "Due"| head -n 1 | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	
	desc_len=${#task_desc}
	project_len=${#task_project}
	dueDate_len=${#task_due_date}

	if [ $desc_len -gt $max_desc_len ]; then
		max_desc_len=$desc_len
	fi


	if [ $project_len -gt $max_project_len ]; then
		max_project_len=$project_len
	fi

	if [ $dueDate_len -gt $max_dueDate_len ]; then
		max_dueDate_len=$dueDate_len
	fi
done


#Now for the real stuff
printf -v padded_col_project "%-$max_project_len.$max_project_len"s "Project"
printf -v padded_col_desc "%-$max_desc_len.$max_desc_len"s "Description"
printf -v padded_col_dueDate "%-$max_dueDate_len.$max_dueDate_len"s "Due"

echo "ID" "$padded_col_project" "$padded_col_desc" "$padded_col_dueDate" "Status"

tasks_ids=$(task _ids)
for task_id in $tasks_ids
do

	printf -v padded_id "%2.2s" $task_id

	task_desc=$(task "$task_id" information | grep "Description" | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	printf -v padded_desc "%-$max_desc_len.$max_desc_len"s "$task_desc"
	task_project=$(task "$task_id" information | grep "Project" | head -n 1 | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	printf -v padded_project "%-$max_project_len.$max_project_len"s "$task_project"
	task_due_date=$(task "$task_id" information | grep "Due"| head -n 1 | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	modified_date=$(echo "$task_due_date" | tr " " '-')
	printf -v padded_dueDate "%-$max_dueDate_len.$max_dueDate_len"s "$modified_date"

	test_active="$(task "$task_id" active verbose=no)"

	if [ "$test_active" ] ; then
		active="  󰬸"
	else
		active="  󰚕"
	fi

	echo "$padded_id" "$padded_project" "$padded_desc" "$padded_dueDate" "$active"
done
