#!/usr/bin/env bash
#              __ _ _    _                 _            
#             / _(_) |  | |               (_)           
#   _ __ ___ | |_ _| |  | | __ _ _ __ _ __ _  ___  _ __ 
#  | '__/ _ \|  _| | |/\| |/ _` | '__| '__| |/ _ \| '__|
#  | | | (_) | | | \  /\  / (_| | |  | |  | | (_) | |   
#  |_|  \___/|_| |_|\/  \/ \__,_|_|  |_|  |_|\___/|_|   
#                                                     
#                                                                                            
# Author: Clément Martinache
#
# A script that generates a rofi menu that uses taskwarrior
# to manage your TODO list.
#
# Inspired by rofi-bluetooth (https://github.com/ClydeDroid/rofi-bluetooth/blob/master/rofi-bluetooth)
#
# Depends on:
#   Arch repositories: rofi, task

# Constants
goback="Back"

#Paths
basePath=$HOME"/.config/rofi/scripts/"

##################
######Getters#####
#################
#check if a task is marked as active
task_started() {
    # WARNING : Potential problem if several tasks active 
    # and one of them has a description which is a substring 
    # of another active task.
    task_desc=$(task "$1" information | grep "Description" | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
    active_tasks=$(task active)
    started=$(echo "$active_tasks" | grep "$task_desc" )

    if [[ -n "$started" ]]; then
        return 0
    else 
        return 1
    fi
}

##################
######Setters#####
##################
#Simple prompt piped to task add xxxxx
add_new_task() {
    prompt_new=$( (echo ) | $rofi_command "󰷥 task add" )

    if [[ -n "$prompt_new" ]]; then 
      task add $prompt_new
      show_menu
    else
      show_menu
      exit
    fi
}

#Simple prompt piped to task add project:xxxx
add_new_task_to_project() {
    project=$1
    prompt_new=$( (echo ) | $rofi_command "󰷥 task add project:""$project" )

    if [[ -n "$prompt_new" ]]; then
      if [[ -n "$project" ]]; then
        task add project:"$project" $prompt_new
        show_menu
      else
        task add project:$prompt_new
        show_menu
      fi
    else
      show_menu
      exit
    fi
}

#Simple prompt piped to task $id modify #####
modify_task() {
    task=$1
    task_id=$(echo "$task" | cut -d ' ' -f 2)
    prompt_modify=$( (echo ) | $rofi_command "󰏫 task "$task" modify" )

    if [[ -n "$prompt_modify" ]]; then 
      task "$task_id" modify $prompt_modify
    else
      exit
    fi
}

delete_task() {
    task=$1
    task_id=$(echo "$task" | cut -d ' ' -f 2)
    task "$task_id" delete
    show_menu
}


mark_task_done() {
    task=$1
    task_id=$(echo "$task" | cut -d ' ' -f 2)
    task "$task_id" done
    show_menu
}

#Toggle active or not
toggle_status_task() {
    task=$1
    task_id=$(echo "$task" | cut -d ' ' -f 2)

    if task_started "$task"; then
        task "$task_id" stop
        show_menu
    else
        task "$task_id" start
        show_menu
    fi
}

##################
#######Menus######
##################
# A submenu for projects. Can add a task, optionally to a new or existing project.
projects_menu() {
    #WARNING : Problem if spaces in project name
    createNewProject="New project"
    projects=$(task projects | tail -n +5 | head -n -2 | cut -d " " -f 1)
    
    options="$createNewProject\n$projects\n$goback\nExit"
    # Open rofi menu, read chosen option
    chosen="$(echo -e "$options" | $rofi_command "Select a project")"
    # Match chosen option to command
    case $chosen in
        "")
            echo "No option chosen."
            ;;
        $createNewProject)
            add_new_task_to_project ""
            ;;
        $goback)
            show_menu
            ;;
        *)
            project=$(task projects | grep -w "$chosen" | cut -d " " -f 1)
            if [[ $project ]]; then add_new_task_to_project "$project"; fi
            ;;
    esac
}
# A submenu for a specific task. Can modify, start/stop the task, delete mark it as done.
task_menu() {

    task=$1
    task_id=$(echo "$task" | xargs | cut -d ' ' -f 1)
    
    echo $task_id

    done="Mark as done"
    if task_started $task_id; then  
        toggle_status="Stop"
    else
        toggle_status="Start"
    fi
    modify="Modify"
    delete="Delete"

    options="$done\n$toggle_status\n$modify\n$delete\n$goback\nExit"

    task_desc=$(task "$task_id" information | grep "Description" | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
	task_project=$(task "$task_id" information | grep "Project" | head -n 1 | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}')
    # TODO : remove brackets if empty project
    prompt_text="Task $task_id - ($task_project) | $task_desc"
    prompt="$(echo -e $prompt_text)"

    # Open rofi menu, read chosen option    
    chosen="$(echo -e "$options" | $rofi_command "$prompt" )"
    # Match chosen option to command
    case $chosen in
        "")
            echo "No option chosen."
            ;;
        $done)
            mark_task_done $task_id
            ;;
        $toggle_status)
            toggle_status_task $task_id
            ;;
        $modify)
            modify_task $task_id
            ;;
        $delete)
            delete_task $task_id
            ;;
        $goback)
            show_menu
            ;;
    esac
}

# Opens a rofi menu with current tasks and possibility to add new tasks
show_menu() {
    source "$basePath"genTaskList.sh > "$basePath"taskList
    legend=$(head "$basePath"taskList -n 1)
    tasks=$(tail  "$basePath"taskList -n +2)
    createNewTask="Add new task"
    addNewTaskToProject="Add new task to project"

    # Options passed to rofi
    options="$createNewTask\n$addNewTaskToProject\n$legend\n$tasks\nExit"
    
    # Open rofi menu, read chosen option
    chosen="$(echo -e "$options" | $rofi_command "󱃕" )"

    # Match chosen option to command
    case $chosen in
        "" | $legend )
            echo "No option chosen."
            ;;
        $createNewTask)
            add_new_task
            ;;
        $addNewTaskToProject)
            projects_menu
            ;;
        *)
            task=$chosen
            # Open a submenu if a task is selected
            if [[ $task ]]; then task_menu "$task"; fi
            ;;
    esac
}

# Rofi command to pipe into, can add any options here
rofi_command="rofi -dmenu -no-fixed-num-lines -matching fuzzy -theme ~/.config/rofi/themes/taskWarrior.rasi -i -p "

case "$1" in
    --status)
        print_status
        ;;
    *)
        show_menu
        ;;
esac
