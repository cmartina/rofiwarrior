# rofiWarrior

Rofi menu to manage taskWarrior tasks.

## Install

Just clone the repo and put the sh files in ~/.config/rofi/scripts, and the theme in ~/.config/rofi/themes

## Use wth i3 

I just add this to my i3 config file :
bindsym $mod+Shift+t exec --no-startup-id ~/.config/rofi/scripts/taskWarrior.sh

## TODO
- Prevent legend from being able to be selected
- Change sorting ?
- Active task color symbol
- Easier way to manage due date/hour
- Columns Padding ("Description" and "Due" should be centered)
 
### Polybar module
- Symbol if active task
- Number of tasks ?
- 